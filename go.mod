module gitlab.com/gitlab-org/security-products/sast/v2

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/bandit/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/common/orchestrator/v2 v2.5.2-0.20200701191834-8bba54cd87c7
	gitlab.com/gitlab-org/security-products/analyzers/common/table/v2 v2.0.0-20200409055313-0cf04bc42167
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.10.3
	gitlab.com/gitlab-org/security-products/analyzers/eslint/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/gosec/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/secrets/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/tslint/v2 v2.2.0
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
)

go 1.13
