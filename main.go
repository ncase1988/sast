package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/orchestrator/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/table/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"

	_ "gitlab.com/gitlab-org/security-products/analyzers/bandit/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/eslint/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gosec/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/tslint/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/plugin"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Perform SAST on given directory (using copy) or on $CI_PROJECT_DIR (mount binding)."
	app.ArgsUsage = "[project-dir]"
	app.Author = "GitLab"

	opts := orchestrator.Options{
		EnvVarPrefix: "SAST_",
		ArtifactName: "gl-sast-report.json",
		PostWrite:    renderTable,
	}

	app.Flags = orchestrator.MakeFlags(opts)
	app.Action = orchestrator.MakeAction(opts)

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

// renderTable renders the vulnerabilities as a plain text table.
func renderTable(report issue.Report) {
	t := table.New([]int{10, 10, 60})
	t.AppendSeparator()
	t.AppendCells("Severity", "Tool", "Location")
	t.AppendSeparator()
	for _, issue := range report.Vulnerabilities {
		location := fmt.Sprintf("%s:%d", issue.Location.File, issue.Location.LineStart)
		t.AppendCells(issue.Severity.String(), issue.Scanner.Name, location)
		t.AppendText("")
		t.AppendText(issue.Message)
		t.AppendSeparator()
	}
	t.Render(os.Stdout)
}
